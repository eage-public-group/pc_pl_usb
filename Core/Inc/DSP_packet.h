/*
 * DSP_packet.h
 *
 *  Created on: Jun 22, 2024
 *      Author: Dell
 */

#ifndef INC_DSP_PACKET_H_
#define INC_DSP_PACKET_H_

#pragma pack(1)

/*
 * Structure for Start Session Packet
 */
typedef struct __DSP_Start_Session
{
	uint16_t 	PacketSize;
	uint8_t 	PacketType;
	uint8_t 	NumChannels;
	uint16_t 	DataType;
	uint16_t 	NumSample;
	uint32_t 	SamplingRate;
}DSP_StartSessTypdef;

/*
 * Structure for Samples Packet
 */
typedef struct __DSP_Samples
{
	uint16_t 	PacketSize;
	uint8_t 	PacketType;
	uint8_t 	channel_num0;
	uint16_t 	num_samples0;
	union
	{
		uint32_t 	Int_data[5];
		float32_t 	Float_data[5];
	}Data0;

	uint8_t 	channel_num1;
	uint16_t 	num_samples1;
	union
	{
		uint32_t 	Int_data[5];
		float32_t 	Float_data[5];
	}Data1;
}DSP_SamplesTypdef;



#endif /* INC_DSP_PACKET_H_ */
